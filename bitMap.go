package main

type bitMap struct {
	keys int
	arr  []uint32
}

func NewBitMap(num uint32) *bitMap {
	return &bitMap{
		arr:  make([]uint32, num/32+1),
		keys: 0,
	}
}
func (b *bitMap) Add(num int) {
	key := num / 32
	position := num % 32

	b.arr[key] |= 1 << position
}

func (b *bitMap) Get(num int) bool {
	key := num / 32
	position := num % 32
	nums := b.arr[key] >> position
	if nums&1 == 1 {
		return true
	}
	return false
}


